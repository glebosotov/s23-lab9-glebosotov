# Lab 9

Website - chat.openai.com (and supporting websites on openai domain)

## [Injection](https://owasp.org/www-project-top-ten/2017/A1_2017-Injection.html)

Test step | Expected Result | Actual Result
--- | --- | ---
Attempt to inject SQL code into a search field (on [help page](https://help.openai.com/en/articles/6825453-chatgpt-release-notes)) | Database error or no results returned | Some results returned, no data breach or any other indication of a successful injection
Attempt to inject SQL code into a login form | Error message or successful login | "Email is not valid." message is shown
Attempt to inject SQL code into a URL parameter | Error message or unexpected behavior | As expected, I am shown search results for query `'; DROP TABLE users; --` and getting message `We couldn't find any articles for:'`

## [Cross-Site Scripting (XSS)](https://owasp.org/www-project-top-ten/2017/A7_2017-Cross-Site_Scripting_(XSS).html)

Test step | Expected Result | Actual Result
--- | --- | ---
Attempt to inject a script into a search field | Script is not executed and/or input is sanitized | `Search results for:<script>alert("XSS Attack!")</script>`
Attempt to inject a script into a support chat | Script is not executed and/or input is sanitized | I get the message `Can you please clarify if you require any assistance with your XSS attack testing or if you are just informing us of your actions?
`
Attempt to inject a script into a URL parameter | Script is not executed and/or input is sanitized | The website still opens with the same parameter, even though the URL is different

## [Session Management](https://owasp.org/www-project-top-ten/2017/A3_2017-Sensitive_Data_Exposure.html)

Test step | Expected Result | Actual Result
--- | --- | ---
Attempt to log in as a user and retrieve their session ID | Session ID is unique and changes on login | The website uses cookies for session management, but session token changes on login
Attempt to maintain access to a session after logging out | Access is denied or redirected to a login page | After attempting to login using a curl request with path `/api/auth/session` I got html page with the message `Checking your browser`. I did not get access to any sensitive data. I also am adding the HTML to this repo.
